require "rails_helper"

RSpec.describe Api::V1::ArticlesController do
  context 'new article success' do
    let(:file_path) { Rails.root.join('articles.json') }
    let(:json) { JSON.parse(File.read(file_path)) }
    subject do
      post :import, format: :json, :_json => json
    end

    it 'creates a new article' do
      subject
      expect(Article.last.title).to eq(json.last["title"])
    end
  end  
end