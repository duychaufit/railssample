class Api::V1::ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]

  def  import
    # binding.pry
    return if params["_json"].nil?
    params["_json"].each do |json|
      article = Article.new
      article["title"] = json["title"]
      article["text"] = json["text"]
      article.save
    end
    render text: 'import complete'
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # def create
  #   # curl -d "article[title]=aaa" -d "article[text]=bbb" http://localhost:3000/api/v1/articles/import
  #   render text: 'create <---------------'
  # end


  # POST /articles
  # POST /articles.json
  def create
    # binding.pry
    @article = Article.new(article_params)
    @article.save
    render text: "aaa"
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :text)
    end
end
