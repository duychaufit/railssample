json.extract! article, :id, :title, :text, :created_at, :updated_at
json.url api_v1_article_url(article, format: :json)
